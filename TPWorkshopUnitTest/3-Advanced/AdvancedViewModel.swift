//
//  ThirdViewModel.swift
//  TPWorkshopUnitTest
//
//  Created by digital.aurum on 19/11/21.
//

import Foundation
import UIKit

class AdvancedViewModel {
    // MARK: Properties
    
    let usecase: AdvancedNetworkProvider
    var products = [Product]()
    
    init(usecase: AdvancedNetworkProvider) {
        self.usecase = usecase
    }
    
    // MARK: Output
    var didReceiveData: (() -> Void)?
    var errorOutput: ((String) -> Void)?
    var clickSideEffectOutput: ((String) -> Void)?
    
    // MARK: Input
    
    func loadData() {
        var result: NetworkResult<ProductResult>?
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        usecase.fetchProduct { productResult in
            result = productResult
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let result = result else {
                self.errorOutput?("found error in network")
                return
            }
            
            switch result {
            case let .success(result):
                self.products = result.data
                self.didReceiveData?()
            case let .failed(message):
                self.errorOutput?(message)
            }
        }
    }
    
    func clickProduct(index: Int) {
        guard products.indices.contains(index) else {
            clickSideEffectOutput?("No product found")
            return
        }
        
        let selectedProduct = products[index]
        clickSideEffectOutput?(selectedProduct.name)
    }
}
