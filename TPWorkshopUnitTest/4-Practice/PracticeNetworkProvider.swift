//
//  PracticeNetworkProvider.swift
//  TPWorkshopUnitTest
//
//  Created by digital.aurum on 23/11/21.
//

import Foundation

struct PracticeUseCase {
    func fetchInspiration(completion: @escaping ((NetworkResult<InspirationResult>) -> Void)){
            guard let url = Bundle.main.path(forResource: "InspirationData", ofType: "json") else {
                completion(.failed("URL Not found"))
                return
            }
            
            if let data = try? Data(contentsOf: URL(fileURLWithPath: url), options: .mappedIfSafe) {
                if let result = try? JSONDecoder().decode(InspirationResult.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failed("Failed when decoding"))
                }
            } else {
                completion(.failed("Failed converting to data"))
            }
        }
    
    
    func fetchProduct(completion: @escaping ((NetworkResult<ProductResult>) -> Void)){
            guard let url = Bundle.main.path(forResource: "ProductData", ofType: "json") else {
                completion(.failed("URL Not found"))
                return
            }
            
            if let data = try? Data(contentsOf: URL(fileURLWithPath: url), options: .mappedIfSafe) {
                if let result = try? JSONDecoder().decode(ProductResult.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failed("Failed when decoding"))
                }
            } else {
                completion(.failed("Failed converting to data"))
            }
        }
    
    
    func fetchTicker(completion: @escaping ((NetworkResult<Ticker>) -> Void)){
        guard let url = Bundle.main.path(forResource: "TickerData", ofType: "json") else {
            completion(.failed("URL Not found"))
            return
        }
        
        if let data = try? Data(contentsOf: URL(fileURLWithPath: url), options: .mappedIfSafe) {
            if let result = try? JSONDecoder().decode(Ticker.self, from: data) {
                completion(.success(result))
            } else {
                completion(.failed("Failed when decoding"))
            }
        } else {
            completion(.failed("Failed converting to data"))
        }
    }

}

protocol PracticeNetworkProvider {
    func fetchInspiration(completion: @escaping (NetworkResult<InspirationResult>) -> Void)
    func fetchProduct(completion: @escaping (NetworkResult<ProductResult>) -> Void)
    func fetchTicker(completion: @escaping ((NetworkResult<Ticker>) -> Void))
}
