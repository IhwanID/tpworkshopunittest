//
//  3-Advanced-Standard.swift
//  TPWorkshopUnitTestTests
//
//  Created by digital.aurum on 18/11/21.
//

import XCTest
@testable import TPWorkshopUnitTest

class __Advanced: XCTestCase {
    // MARK: - Product Assertion Tests
    
    func test_single_struct_notEqual() {
        let productResult = Product(
            id: 1,
            name: "lorem ipsum",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        let expectedResult = Product(
            id: 2,
            name: "lorem ipsum - 2",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        XCTAssertNotEqual(productResult, expectedResult)
    }
    
    func test_single_struct_equal() {
        let productResult = Product(
            id: 1,
            name: "lorem ipsum",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        let expectedResult = Product(
            id: 1,
            name: "lorem ipsum",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        XCTAssertEqual(productResult, expectedResult)
    }
    
    func test_array_struct() {
        let firstProduct = Product(
            id: 1,
            name: "lorem ipsum",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        let secondProduct = Product(
            id: 2,
            name: "lorem ipsum - 2",
            imageURL: nil,
            price: "Rp 10.000",
            shop: Shop(id: 10, name: "Toko Serba Ada", location: "Bandung")
        )
        
        let products = [firstProduct, secondProduct]
        let expectedProducts = [secondProduct, firstProduct]
        XCTAssertNotEqual(products, expectedProducts)
    }
    
    // MARK: - LOAD DATA TESTS
    
    func test_positive_loadData() {
        let mockUsecase = MockPositiveWorkshopProvider()
        let viewModel = AdvancedViewModel(usecase: mockUsecase)
        
        let expectation = expectation(description: "Should return products")
        
        viewModel.didReceiveData = {
            XCTAssertEqual(viewModel.products, MockProductData.generateProductResult().data)
            expectation.fulfill()
        }
        
        viewModel.loadData()
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_negative_loadData() {
        let mockUsecase = MockNegativeWorkshopProvider()
        let viewModel = AdvancedViewModel(usecase: mockUsecase)
        
        let expectation = expectation(description: "Should return products")
        
        viewModel.errorOutput = { result in
            XCTAssertEqual(result, "No internet connection")
            expectation.fulfill()
        }
        
        viewModel.loadData()
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_positive_click_product() {
        let mockUsecase = MockPositiveWorkshopProvider()
        let viewModel = AdvancedViewModel(usecase: mockUsecase)
        
        let loadDataExpectation = expectation(description: "Should return products")
        let clickProductExpectation = expectation(description: "Should return product name")
        
        viewModel.didReceiveData = {
            XCTAssertEqual(viewModel.products, MockProductData.generateProductResult().data)
            loadDataExpectation.fulfill()
        }
        
        viewModel.loadData()
        wait(for: [loadDataExpectation], timeout: 1)
        
        let selectedProduct = MockProductData.generateProductResult().data.first!
        
        viewModel.clickSideEffectOutput = { productName in
            XCTAssertEqual(productName, selectedProduct.name)
            clickProductExpectation.fulfill()
        }
        
        viewModel.clickProduct(index: 0)
        wait(for: [clickProductExpectation], timeout: 1)
    }
    
    func test_negative_click_product() {
        let mockUsecase = MockEmptyWorkshopProvider()
        let viewModel = AdvancedViewModel(usecase: mockUsecase)
        
        let loadDataExpectation = expectation(description: "Should return empty products")
        let clickProductExpectation = expectation(description: "Should return empty product name")
        
        viewModel.didReceiveData = {
            XCTAssertEqual(viewModel.products, [])
            loadDataExpectation.fulfill()
        }
        
        viewModel.loadData()
        wait(for: [loadDataExpectation], timeout: 1)
        
        viewModel.clickSideEffectOutput = { productName in
            XCTAssertEqual(productName, "No product found")
            clickProductExpectation.fulfill()
        }
        
        viewModel.clickProduct(index: 0)
        wait(for: [clickProductExpectation], timeout: 1)
    }
}
