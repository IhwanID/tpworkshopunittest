//
//  4-Intermediate.swift
//  TPWorkshopUnitTestTests
//
//  Created by digital.aurum on 24/11/21.
//

import XCTest
@testable import TPWorkshopUnitTest

class __Practice: XCTestCase {    
    internal func assertArrayHashDiffable(expectedResult: [HashDiffable], values: [HashDiffable]) {
        XCTAssertEqual(expectedResult.count, values.count, "An array count mismatch.")
        if expectedResult.count == values.count {
            expectedResult.enumerated().forEach { index, element in
                XCTAssertTrue(element.isEqual(to: values[index]), "expected index \(index) equal to \(type(of: values[index])) but get \(type(of: element)) instead")
            }
        }
    }
    
    func test_positiveLoadData() {
        let mockUsecase = Mock4PositiveWorkshopProvider()
        let viewModel = PracticeViewModel(useCase: mockUsecase)
        
        let loadDataExpectation = expectation(description: "Should return products")
        
        viewModel.didReceiveData = {
            self.assertArrayHashDiffable(expectedResult: Mock4ProductData.generateExpectedCompleteValue(), values: viewModel.data)
            loadDataExpectation.fulfill()
        }
        
        viewModel.onDidLoad()
        wait(for: [loadDataExpectation], timeout: 1)
        
    }
    
    func test_fireDate() {
            var tickerMockData: Ticker?
            
            let useCase = Mock4PositiveWorkshopProvider()
            let viewModel = PracticeViewModel(useCase: useCase, timerProvider: MockTimer.self)
            let firstExpectation = expectation(description: "should return complete data without ticker")
            let secondExpectation = expectation(description: "should return complete data with ticker")
            
            PracticeEnvironment.saveTickerCache = { _, _ in
                tickerMockData = Mock4ProductData.generateTicker()
            }
            
            PracticeEnvironment.loadTickerCache = { _ in
                tickerMockData
            }
            
            viewModel.didReceiveData = {
                self.assertArrayHashDiffable(expectedResult: viewModel.data,
                                        values: Mock4ProductData.generateExpectedCompleteValue())
                firstExpectation.fulfill()
            }
            
            viewModel.onDidLoad()
            wait(for: [firstExpectation], timeout: 1)
            
            guard let ticker = PracticeEnvironment.loadTickerCache("") else {
                XCTAssertNil("found nil on ticker cache")
                return
            }
            
            var newExpectedData = Mock4ProductData.generateExpectedCompleteValue()
            newExpectedData.insert(ticker, at: 0)
            
            viewModel.didReceiveData = {
                self.assertArrayHashDiffable(expectedResult: viewModel.data,
                                        values: newExpectedData)
                secondExpectation.fulfill()
            }
            
            viewModel.onFireDate()
            MockTimer.currentTimer.fire()
            wait(for: [secondExpectation], timeout: 1)
        }
    
}

